# Update tag according to the latest tag:
# https://gitlab.cern.ch/webservices/discourse-cern/container_registry
FROM gitlab-registry.cern.ch/webservices/discourse-cern:v2.5.0.beta4

RUN apt -y install ruby-mysql2 libmariadb-dev

### For migration purposes
# Add php-serialize gem
RUN echo "gem 'php-serialize'" >> Gemfile
RUN echo "gem 'mysql2'" >> Gemfile
# Remove the /discourse/Gemfile freeze
RUN bundle config unset deployment
RUN bundle config --delete frozen
RUN bundle update --source php-serialize
RUN bundle update --source mysql2

### Gem installation
RUN exec bundle install --deployment --jobs 4 --without test --without development && \
    exec bundle exec rake maxminddb:get && \
    find /discourse/vendor/bundle -name tmp -type d -exec rm -rf {} +

### Infinite loop
CMD ["sh","-c","while true; do foo; sleep 2; done"]